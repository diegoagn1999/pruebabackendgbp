<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class usuariosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'foto' => $this->faker->url(),
            'estado' => $this->faker->randomDigit(),
            'created_by' => $this->faker->randomDigitNotNull(),
            'update_by' => $this->faker->randomDigitNotNull(),
        ];
    }
}
