<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class productosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'descripcion' => $this->faker->sentence(),
            'estado' => $this->faker->randomDigit(),
            'created_by' => $this->faker->randomDigitNotNull(),
            'update_by' => $this->faker->randomDigitNotNull(),
        ];
    }
}
