<?php

namespace Database\Factories;

use App\Models\Usuarios;
use Illuminate\Database\Eloquent\Factories\Factory;

class bodegasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'id_responsable' => Usuarios::orderByRaw('RAND()')->first(),
            'estado' => $this->faker->randomDigit(),
            'created_by' => $this->faker->randomDigitNotNull(),
            'update_by' => $this->faker->randomDigitNotNull(),
        ];
    }
}
