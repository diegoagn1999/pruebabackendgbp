<?php

namespace Database\Factories;

use App\Models\Bodegas;
use App\Models\inventarios;
use Illuminate\Database\Eloquent\Factories\Factory;

class historialesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cantidad' => $this->faker->randomDigit(),
            'id_bodega_destino' => Bodegas::orderByRaw('RAND()')->first(),
            'id_bodega_origen' => Bodegas::orderByRaw('RAND()')->first(),
            'id_inventario' => inventarios::orderByRaw('RAND()')->first(),
            'created_by' => $this->faker->randomDigitNotNull(),
            'update_by' => $this->faker->randomDigitNotNull(),
        ];
    }
}
