<?php

namespace Database\Factories;

use App\Models\Bodegas;
use App\Models\Productos;
use Illuminate\Database\Eloquent\Factories\Factory;

class inventariosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_bodega' => Bodegas::orderByRaw('RAND()')->first(),
            'id_producto' => Productos::orderByRaw('RAND()')->first(),
            'cantidad' => $this->faker->randomNumber('2', false),
            'created_by' => $this->faker->randomDigitNotNull(),
            'update_by' => $this->faker->randomDigitNotNull(),
        ];
    }
}
