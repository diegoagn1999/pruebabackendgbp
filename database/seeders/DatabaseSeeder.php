<?php

namespace Database\Seeders;

use App\Models\Bodegas;
use App\Models\historiales;
use App\Models\inventarios;
use App\Models\Productos;
use App\Models\Usuarios;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Usuarios::factory(10)->create();
        Productos::factory(10)->create();
        Bodegas::factory(10)->create();
        inventarios::factory(10)->create();
        historiales::factory(10)->create();
    }
}
