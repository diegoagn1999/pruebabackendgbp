<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodegasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Bodegas', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('nombre', 30);
            $table->integer('id_responsable')->length(11);
            $table->tinyInteger('estado')->length(1);
			$table->integer('created_by')->length(11);
			$table->integer('update_by')->length(11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Bodegas');
    }
}
