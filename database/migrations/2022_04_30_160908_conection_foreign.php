<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConectionForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bodegas', function (Blueprint $table) {
            $table->foreign('id_responsable')->references('id')->on('Usuarios')->onDelete('cascade');
        });
        Schema::table('inventarios', function (Blueprint $table) {
            $table->foreign('id_producto')->references('id')->on('Productos')->onDelete('cascade');
            $table->foreign('id_bodega')->references('id')->on('Bodegas')->onDelete('cascade');
        });
        Schema::table('historiales', function (Blueprint $table) {
            $table->foreign('id_bodega_origen')->references('id')->on('Bodegas')->onDelete('cascade');
            $table->foreign('id_bodega_destino')->references('id')->on('Bodegas')->onDelete('cascade');
            $table->foreign('id_inventario')->references('id')->on('inventarios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
