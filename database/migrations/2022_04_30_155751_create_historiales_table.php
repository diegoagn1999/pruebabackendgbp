<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistorialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historiales', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('cantidad')->length(11);
            $table->integer('id_bodega_origen')->length(11);
            $table->integer('id_bodega_destino')->length(11);
            $table->integer('id_inventario')->length(11);
			$table->integer('created_by')->length(11);
			$table->integer('update_by')->length(11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historiales');
    }
}
