<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BodegaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'required',
            'id_responsable'=>'required | numeric',
            'estado'=>'required | numeric',
            'created_by'=>'required | numeric',
            'update_by'=>'required | numeric',
        ];
    }
    
    public function messages(){
        return[
            'nombre.required'=>'El campo nombre es requerido',
            'id_responsable.required'=>'El campo id_responsable es requerido',
            'id_responsable.numeric'=>'El campo id_responsable debe ser un numero',
            'estado.required'=>'El campo estado es requerido',
            'estado.numeric'=>'El campo estado debe ser un numero',
            'created_by.required'=>'El campo created_by es requerido',
            'created_by.numeric'=>'El campo created_by debe ser un numero',
            'update_by.required'=>'El campo update_by es requerido',
            'update_by.numeric'=>'El campo update_by debe ser un numero',
            
        ];
    }
}
