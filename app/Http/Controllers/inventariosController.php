<?php

namespace App\Http\Controllers;

use App\Models\historiales;
use App\Models\inventarios;
use Illuminate\Http\Request;

class inventariosController extends Controller
{
    public function createOrUpdate(Request $request){
        $inventario = inventarios::where('id_bodega', $request['id_bodega'])->where('id_producto', $request['id_producto']);
        if($this->comprobarFalloDuplicacion($inventario)){
            return "Se encontró mas de un registro en la db, contacte con soporte para corregir este error";
        }
        if($inventario->count() === 1){
            $inventario->first();
            $inventarios = new inventarios();
            $inventarios->id_bodega = $request['id_bodega'];
            $inventarios->id_producto = $request['id_producto'];
            $inventarios->cantidad = $request['cantidad'];
            $inventarios->created_by = $request['created_by'];
            $inventarios->update_by = $request['update_by'];
            if($inventarios->save()){
                return "inventario actualizado correctamente";
            }

        }
        
        $inventarios = new inventarios();
        $inventarios->id_bodega = $request['id_bodega'];
        $inventarios->id_producto = $request['id_producto'];
        $inventarios->cantidad = $request['cantidad'];
        $inventarios->created_by = $request['created_by'];
        $inventarios->update_by = $request['update_by'];
        if($inventarios->save()){
            return "inventario guardado correctamente";
        }
        return "error al guardar inventario";
    }

    public function moving(Request $request){
        $inventarioBodegaA = inventarios::where('id_producto', $request['idProduct'])->where('id_bodega', $request['bodegaA']);
        if($this->comprobarFalloDuplicacion($inventarioBodegaA)){
            return "error de registro duplicado o inexistente en la db en la bodega con id ".$request['bodegaB']. "y producto con id ".$request['idProduct'];
        }
        $inventarioBodegaA = $inventarioBodegaA->first();
        if($inventarioBodegaA->cantidad < $request['cantProduct']){
            return "Error. está intentando sacar mas productos de los que hay en Bodega";
        }

        $inventarioBodegaB = inventarios::where('id_producto', $request['idProduct'])->where('id_bodega', $request['bodegaB']);
        if($this->comprobarFalloDuplicacion($inventarioBodegaB)){
            return "error de registro duplicado o inexistente en la db en la bodega con id ".$request['bodegaB']. " y producto con id ".$request['idProduct'];
        }
        $inventarioBodegaB = $inventarioBodegaB->first();
        $inventarioBodegaB->cantidad += $request['cantProduct'];
        $inventarioBodegaA->cantidad -= $request['cantProduct'];
        $inventarioBodegaB->save();
        $inventarioBodegaA->save();
        $historiales = new historiales();
        $historiales->cantidad = $request['cantProduct'];
        $historiales->id_bodega_origen = $request['bodegaA'];
        $historiales->id_bodega_destino = $request['bodegaB'];
        $historiales->id_inventario = $inventarioBodegaB->id;
        $historiales->created_by = $request['created_by'];
        $historiales->update_by = $request['update_by'];
        
        if(!$historiales->save()){
            return "problemas al crear el historial";
        }
        return "se ha generado la transaccion correctamente";
    }

    public function comprobarFalloDuplicacion($registro){
        if(1 === $registro->count()){
            return false;
        }
        return true;
    }
}
