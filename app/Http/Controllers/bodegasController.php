<?php

namespace App\Http\Controllers;

use App\Http\Requests\BodegaRequest;
use App\Models\Bodegas;
use Illuminate\Http\Request;

class bodegasController extends Controller
{
    public function listBodegas(){
        return Bodegas::orderBy('nombre', 'asc')->get();
    }

    public function store(BodegaRequest $request){
        // se debe ingresar un campo json de la siguiente manera
        // {
        //     "nombre" : "nombreEjemplo",
        //     "id_responsable" : 6, // debe ser un numero existente en la tabla usuarios
        //     "estado" : 1,
        //     "created_by" : 4,
        //     "update_by" : 5
        // }
        $bodega = new Bodegas();
        $bodega->nombre = $request['nombre'];
        $bodega->id_responsable = $request['id_responsable'];
        $bodega->estado = $request['estado'];
        $bodega->created_by = $request['created_by'];
        $bodega->update_by = $request['update_by'];
        if($bodega->save()){
            return "La bodega ha sido creada con exito";
        }
        return "Error al crear la Bodega";
        

    }
}
