<?php

namespace App\Http\Controllers;

use App\Models\inventarios;
use App\Models\Productos;
use Illuminate\Http\Request;

class productosController extends Controller
{
    public function listProductTotal($idProducto){
        $cantidadProductos = inventarios::where('id_producto', $idProducto)->get('cantidad');
        $cantidadTotal = 0;
        foreach($cantidadProductos as $producto){
            $cantidadTotal += $producto->cantidad;
        }
        return $cantidadTotal;
    }

    public function store(Request $request){
        // {
        //     "nombre" : "nombreEjemplo",
        //     "descripcion" : "descripcion de ejemplo", 
        //     "estado" : 1,
        //     "created_by" : 4,
        //     "update_by" : 5
        // }
        $bodegaDefault = 1;
        $cantidadInicialProductoDefault = 10;
        $producto = new Productos();
        $producto->nombre = $request['nombre'];
        $producto->descripcion = $request['descripcion'];
        $producto->estado = $request['estado'];
        $producto->created_by = $request['created_by'];
        $producto->update_by = $request['update_by'];
        if($producto->save()){
            $inventario = new inventarios();
            $inventario->id_bodega = $bodegaDefault;
            $inventario->id_producto = $producto->id;
            $inventario->cantidad = $cantidadInicialProductoDefault;
            $inventario->created_by = $request['created_by'];
            $inventario->update_by = $request['update_by'];
            if($inventario->save()){
                return "El producto ha sido creada con exito con una cantidad de ".$cantidadInicialProductoDefault." en al bodega con id ".$bodegaDefault;
            }
            return "se ha creado el producto correctamente pero no se podido hacer un registro en inventario";
        }
        return "Error al crear la Producto";
    }
}
