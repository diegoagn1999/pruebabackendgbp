<?php

use App\Http\Controllers\bodegasController;
use App\Http\Controllers\inventariosController;
use App\Http\Controllers\productosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/listBodegas', [bodegasController::class, 'listBodegas']);
Route::get('/crearBodega', [bodegasController::class, 'store']);
Route::get('/listadoProductosTotal/{idProducto}', [productosController::class, 'listProductTotal']);
Route::get('/crearProducto', [productosController::class, 'store']);
Route::get('/crearInventario', [inventariosController::class, 'createOrUpdate']);
Route::get('/moverProducto', [inventariosController::class, 'moving']);
